<?hh //strict

  class :cgf:misc:gist extends :x:element {

    attribute
      string gist-id @required;

    protected function render(): :xhp {
      return
        <x:frag>
          <script src={'https://gist.github.com/' . $this->getAttribute('gist-id') . '.js'}></script>
        </x:frag>;
    }
  }