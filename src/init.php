<?hh //strict

  // Commonly used components
  require_once 'common/header.php';
  require_once 'common/open-graph.php';
  require_once 'common/app-links.php';

  // Social Components (FB App, Like Button etc.)
  require_once 'social/facebook-app.php';
  require_once 'social/facebook-like-button.php';
  require_once 'social/facebook-share-button.php';
  require_once 'social/facebook-like-box.php';
  require_once 'social/facebook-send-button.php';
  require_once 'social/facebook-embedded-post.php';

  // Commetning Tools
  require_once 'social/disqus/disqus-comments.php';

  // Text
  require_once 'text/limit.php';

  // Misc
  require_once 'misc/gist.php';