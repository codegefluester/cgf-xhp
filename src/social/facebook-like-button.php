<?hh //strict
  
  class :cgf:social:facebook-like-button extends :x:element {
    
    attribute
      enum { "like", "recommend" } action = "like",
      enum { "light", "dark" } colorscheme = "light",
      string href,
      enum { "standard", "button_count", "button", "count" } layout = "standard",
      string ref,
      boolean share = false,
      boolean show-faces = false,
      int width;
      
    protected function render(): :xhp {
      return 
        <x:frag>
          <div class="fb-like" 
            data-href={$this->getAttribute('href')} 
            data-layout={$this->getAttribute('layout')} 
            data-action={$this->getAttribute('action')} 
            data-show-faces={$this->getAttribute('show-faces')} 
            data-share={$this->getAttribute('share')}
            data-width={$this->getWidth()}
          />
        </x:frag>;  
    }
    
    private function getWidth(): int {
      if ($this->getAttribute('width')) {
        return $this->getAttribute('width');
      }
      
      switch ($this->getAttribute('layout')) {
        
        case 'standard':
          return 450;
          break;
        case 'box_count':
          return 55;
          break;
        case 'button_count':
          return 90;
          break;
        case 'button':
          return 47;
          break;
      } 
    }
  }