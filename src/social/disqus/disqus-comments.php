<?hh // strict
  
  class :cgf:social:disqus extends :x:element {
    
    attribute
      string shortname @required;
      
    protected function render(): :xhp {
      $js = 
        <script>
          {'var disqus_shortname = \''.$this->getAttribute('shortname').'\';
            (function() {
            var dsq = document.createElement(\'script\'); dsq.type = \'text/javascript\'; dsq.async = true;
            dsq.src = \'//\' + disqus_shortname + \'.disqus.com/embed.js\';
            (document.getElementsByTagName(\'head\')[0] || document.getElementsByTagName(\'body\')[0]).appendChild(dsq);
        })();'}
        </script>;
      
      return 
        <x:frag>
          <div id="disqus_thread"></div>
          {$js}
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </x:frag>;
    }
  }