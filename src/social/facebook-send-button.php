<?hh //strict
  
  class :cgf:social:facebook-send-button extends :x:element {
    
    attribute
      string href @required,
      enum {"light", "dark"} colorscheme = "light",
      boolean kid-directed-site = false,
      string ref;
      
    
    protected function render(): :xhp {
      return 
        <x:frag>
          <div 
            class="fb-send"
            data-href={$this->getAttribute('href')}
            data-colorscheme={$this->getAttribute('colorscheme')}
            data-kid-directed-site={$this->getAttribute('kid-directed-site')}
            data-ref={$this->getAttribute('ref')}
          />
        </x:frag>;
    }
  }