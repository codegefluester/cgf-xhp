<?hh //strict
  
  class :cgf:social:facebook-app extends :x:element {
    
    attribute
      string app-id @required,
      boolean xfbml = true,
      string version = 'v2.2',
      string locale = 'en_US';
    
    protected function render(): :xhp {
      
      $js = 
        <script>
          {'window.fbAsyncInit = function() {
              FB.init({
                appId      : \''.$this->getAttribute('app-id').'\',
                xfbml      : '.$this->getAttribute('xfbml').',
                version    : \''.$this->getAttribute('version').'\'
              });
            };

            (function(d, s, id){
               var js, fjs = d.getElementsByTagName(s)[0];
               if (d.getElementById(id)) {return;}
               js = d.createElement(s); js.id = id;
               js.src = "//connect.facebook.net/'.$this->getAttribute('locale').'/sdk.js";
               fjs.parentNode.insertBefore(js, fjs);
             }(document, \'script\', \'facebook-jssdk\'));'}
        </script>;
      
      return 
        <x:frag>
          <div id="fb-root"></div>
          {$js}
        </x:frag>;
    }
    
    
  }