<?hh //strict
  
  class :cgf:social:facebook-like-box extends :x:element {
    
    attribute
      string href,
      enum {"light", "dark"} colorscheme = "light",
      boolean force-wall = false,
      boolean header = true,
      boolean show-border = true,
      boolean force-wall = false,
      boolean show-faces = true,
      boolean stream = false,
      int width = 300,
      int height = 300;
      
    protected function render(): :xhp {
      return 
        <x:frag>
          <div 
            class="fb-like-box" 
            data-href={$this->getAttribute('href')}
            data-width={$this->getAttribute('width')}
            data-height={$this->getAttribute('height')} 
            data-colorscheme={$this->getAttribute('colorscheme')} 
            data-show-faces={$this->getAttribute('show-faces')} 
            data-header={$this->getAttribute('header')}
            data-stream={$this->getAttribute('stream')}
            data-show-border={$this->getAttribute('show-border')}
            data-force-wall={$this->getAttribute('force-wall')}
          />
        </x:frag>;  
    }
  }