<?hh //strict
  
  class :cgf:social:facebook-embedded-post extends :x:element {
    
    attribute
      string href @required,
      int width = 350;
      
    
    protected function render(): :xhp {
      return 
        <x:frag>
          <div 
            class="fb-post" 
            data-href={$this->getAttribute('href')} 
            data-width={$this->validWidth($this->getAttribute('width'))}
          />
        </x:frag>;
    }
    
    /*
    * Validates the width attribute and returns a valid min/max value
    */
    private function validWidth(int $width): int {
      if ($width < 350) return 350;
      if ($width > 750) return 750;
    }
  }