<?hh //strict
  
  class :cgf:social:facebook-share-button extends :x:element {
    
    attribute
      string href,
      enum { "icon_link", "button_count", "box_count", "button", "link" } layout = "icon_link";
      
    protected function render(): :xhp {
      return 
        <x:frag>
          <div 
            class="fb-share-button" 
            data-href={$this->getAttribute('href')} 
            data-layout={$this->getAttribute('layout')} 
          />
        </x:frag>;  
    }
  }