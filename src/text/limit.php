<?hh // strict
  
  class :cgf:text:limit extends :x:element {
    
    attribute
      int characters = 200 @required;
      
    protected function render(): :xhp {
      $original = $this->getFirstChild();
      $characters = $this->getAttribute('characters');
      
      if ($original) {
        $cleaned = substr(strip_tags($original), 0, $characters);
        if (strlen($original) > $characters) {
          $cleaned = rtrim($cleaned).'...';
        }
      }
      
      return 
        <x:frag>
          {$cleaned}
        </x:frag>;
    }
    
  }