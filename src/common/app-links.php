<?hh //strict
  
  class :cgf:common:header:app-links extends :x:element {
    
    attribute
      string ios-url,
      string ios-id,
      string ios-name,
      string iphone-url,
      string iphone-id,
      string iphone-name,
      string ipad-url,
      string ipad-id,
      string ipad-name,
      string android-url,
      string android-package,
      string android-class,
      string android-app-name,
      string windows-phone-url,
      string windows-phone-id,
      string windows-phone-name,
      string windows-url,
      string windows-id,
      string windows-name,
      string windows-universal-url,
      string windows-universal-id,
      string windows-universal-name,
      boolean web-should-fallback,
      string web-url;
    
    children (:meta)*;
    
    protected function render(): :xhp {
      
      $ios = null;
      if ($this->getAttribute('ios-url')) {
        $ios = $this->getPlatformTags('ios');
      }
      
      $iphone = null;
      if ($this->getAttribute('iphone-url')) {
        $iphone = $this->getPlatformTags('iphone');
      }
      
      $ipad = null;
      if ($this->getAttribute('ipad-url')) {
        $ipad = $this->getPlatformTags('ipad');
      }
      
      $android = null;
      if ($this->getAttribute('android-url')) {
        $android = $this->getPlatformTags('android');
      }
      
      $winphone = null;
      if ($this->getAttribute('windows-phone-url')) {
        $winphone = $this->getPlatformTags('windows-phone');
      }
      
      $windows = null;
      if ($this->getAttribute('windows-url')) {
        $windows = $this->getPlatformTags('windows');
      }
      
      $windows_universal = null;
      if ($this->getAttribute('windows-universal-url')) {
        $windows_universal = $this->getPlatformTags('windows-universal');
      }
      
      $web = null;
      if ($this->getAttribute('web-url')) {
        $web = $this->getPlatformTags('web');
      }
      
      return
        <x:frag>
          {$ios}
          {$iphone}
          {$ipad}
          {$android}
          {$winphone}
          {$windows}
          {$windows_universal}
          {$web}
        </x:frag>;
      
    }
    
    private function getPlatformTags(string $platform): :xhp {
      
      switch ($platform) {
        
        case 'ios':
          return 
            <x:frag>
              <meta property="al:ios:url" content={$this->getAttribute('ios-url')} />
              <meta property="al:ios:app_store_id" content={$this->getAttribute('ios-id')} />
              <meta property="al:ios:app_name" content={$this->getAttribute('ios-name')} />
            </x:frag>;
          break;
        case 'iphone':
          return 
            <x:frag>
              <meta property="al:iphone:url" content={$this->getAttribute('iphone-url')} />
              <meta property="al:iphone:app_store_id" content={$this->getAttribute('iphone-id')} />
              <meta property="al:iphone:app_name" content={$this->getAttribute('iphone-name')} />
            </x:frag>;
          break;
        case 'ipad':
          return 
            <x:frag>
              <meta property="al:ipad:url" content={$this->getAttribute('ipad-url')} />
              <meta property="al:ipad:app_store_id" content={$this->getAttribute('ipad-id')} />
              <meta property="al:ipad:app_name" content={$this->getAttribute('ipad-name')} />
            </x:frag>;
          break;
        case 'android':
          return 
            <x:frag>
              <meta property="al:android:url" content={$this->getAttribute('android-url')} />
              <meta property="al:android:package" content={$this->getAttribute('android-package')} />
              <meta property="al:android:class" content={$this->getAttribute('android-class')} />
              <meta property="al:android:app_name" content={$this->getAttribute('android-name')} />
            </x:frag>;
          break;
        case 'windows-phone':
          return 
            <x:frag>
              <meta property="al:windows_phone:url" content={$this->getAttribute('windows-phone-url')} />
              <meta property="al:windows_phone:app_id" content={$this->getAttribute('windows-phone-id')} />
              <meta property="al:windows_phone:app_name" content={$this->getAttribute('windows-phone-name')} />
            </x:frag>;
          break;
        case 'windows':
          return 
            <x:frag>
              <meta property="al:windows:url" content={$this->getAttribute('windows-url')} />
              <meta property="al:windows:app_id" content={$this->getAttribute('windows-id')} />
              <meta property="al:windows:app_name" content={$this->getAttribute('windows-name')} />
            </x:frag>;
          break;
        case 'windows-universal':
          return 
            <x:frag>
              <meta property="al:windows_universal:url" content={$this->getAttribute('windows-universal-url')} />
              <meta property="al:windows_universal:app_id" content={$this->getAttribute('windows-universal-id')} />
              <meta property="al:windows_universal:app_name" content={$this->getAttribute('windows-universal-name')} />
            </x:frag>;
          break;
        case 'web':
          return 
            <x:frag>
              <meta property="al:web:url" content={$this->getAttribute('web-url')} />
              <meta property="al:web:should_fallback" content={$this->getAttribute('web-should-fallback')} />
            </x:frag>;
          break;
        default:
          break;
      }
      
    }
    
  }