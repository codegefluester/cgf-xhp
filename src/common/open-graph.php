<?hh //strict
  
  class :cgf:common:header:open-graph extends :x:element {
    
    attribute
      string title @required,
      string type @required,
      string image @required,
      string url @required,
      string description,
      string audio,
      string determiner,
      string locale,
      array alternate-locales,
      string site-name,
      string video;
      
    protected function render(): :xhp {
      $audio = null;
      if ($this->getAttribute('audio')) {
        $audio = <meta property="og:audio" content={$this->getAttribute('audio')} />;
      }
      
      $description = null;
      if ($this->getAttribute('description')) {
        $description = <meta property="og:description" content={$this->getAttribute('description')} />;
      }
      
      $determiner = null;
      if ($this->getAttribute('determiner')) {
        $determiner = <meta property="og:determiner" content={$this->getAttribute('determiner')} />;
      }
      
      $locale = null;
      if ($this->getAttribute('locale')) {
        $locale = <meta property="og:locale" content={$this->getAttribute('locale')} />;
      }
      
      $alt_locale_tags = null;
      if ($this->getAttribute('alternate-locales')) {
        $alternates = $this->getAttribute('alternate-locales');
        $alt_locale_tags = <x:frag />;
        array_map(function($aLocale) use ($alt_locale_tags) {
          $alt_locale_tags->appendChild(
            <meta property="og:locale:alternate" content={$aLocale} />
          );
        }, $alternates);
      }
      
      $video = null;
      if ($this->getAttribute('video')) {
        $video = <meta property="og:video" content={$this->getAttribute('video')} />;
      }
      
      $site_name = null;
      if ($this->getAttribute('site-name')) {
        $site_name = <meta property="og:site_name" content={$this->getAttribute('site-name')} />;
      }
      
      return 
        <x:frag>
          <meta property="og:type" content={$this->getAttribute('type')} />
          <meta property="og:title" content={$this->getAttribute('title')} />
          <meta property="og:url" content={$this->getAttribute('url')} />
          <meta property="og:image" content={$this->getAttribute('image')} />
          {$description}
          {$audio}
          {$determiner}
          {$locale}
          {$alt_locale_tags}
          {$video}
        </x:frag>;
    }
    
    
  }