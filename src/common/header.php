<?hh //strict
  
  class :cgf:common:header extends :x:element {
    
    attribute
      string title,
      string description,
      string keywords,
      string charset = 'utf-8',
      string width = 'device-width',
      int initial-scale = 1,
      string user-scalable = 'no';
      
    children (:meta|:style|:title|:script|:link|:cgf:common:header:open-graph|:cgf:common:header:app-links)*;
    
    protected function render(): :xhp {
      return 
        <head>
          <meta charset={$this->getAttribute('charset')} />
          <title>{$this->getAttribute('title')}</title>
          <meta property="description" content={$this->getAttribute('description')} />
          <meta property="keywords" content={$this->getAttribute('keywords')} />
          <meta name="viewport" content={"width=".$this->getAttribute('width').", initial-scale=".$this->getAttribute('initial-scale').", user-scalable=".$this->getAttribute('user-scalable')} />
          {$this->getChildren()}
        </head>;
    }
    
    
  }