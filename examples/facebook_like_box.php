<?hh //strict
  require_once '../../../../vendor/autoload.php';
  
  echo 
    <html>
      <cgf:common:header title="Facebook Like Box Example" description="this is the description" keywords="one, two, three, four" />
      <body>
        <cgf:social:facebook-app app-id="728352667279924" />
        <cgf:social:facebook-like-box 
          href="https://www.facebook.com/FacebookDevelopers"
        />
      </body>
    </html>;