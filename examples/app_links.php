<?hh //strict
  require_once '../../../../vendor/autoload.php';

  echo
    <html>
      <cgf:common:header
        title="CGF XHP AppLinks Example"
        description="this is the description"
        keywords="one, two, three, four">

        <cgf:common:header:app-links
          ios-url="applinks://"
          ios-id="1234567890"
          ios-name="Example App Name"
        />

      </cgf:common:header>
      <body>
      </body>
    </html>;
