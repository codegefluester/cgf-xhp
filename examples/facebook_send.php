<?hh //strict
  require_once '../../../../vendor/autoload.php';
  
  echo 
    <html>
      <cgf:common:header title="Facebook Send Button Example" description="this is the description" keywords="one, two, three, four" />
      <body>
        <cgf:social:facebook-app app-id="728352667279924" />
        <cgf:social:facebook-send-button 
          href="http://codegefluester.de/blog/vendor/codegefluester/xhp-base/examples/facebook_send.php"
        />
      </body>
    </html>;