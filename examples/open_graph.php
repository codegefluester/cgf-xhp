<?hh //strict
  require_once '../../../../vendor/autoload.php';
  
  echo 
    <html>
      <cgf:common:header
        title="CGF XHP Open Graph Example" 
        description="this is the description" 
        keywords="one, two, three, four">
        
        <cgf:common:header:open-graph
          title="Open Graph Title"
          url="http://example.com"
          image="http://example.com/ogimage.png"
          description="The Open Graph Description"
          type="website"
          locale="en_US"
          site-name="codegefluester xhp"
        />
        
      </<cgf:common:header>
      <body>
      </body>
    </html>;